# Java Course | #01 Development ENV Setup & Hello-world example (BASIC)

[![Java Programming Lesson 1](http://img.youtube.com/vi/Bxkwt963gys/0.jpg)](https://youtu.be/Bxkwt963gys)

*__Kindly Like, Subscribe and Share the YouTube Video__*

## Java Course - Lesson 1

Welcome to Java Course (Lesson 1), in this session, we will perform a simple analysis between different IDEs, e.g.
Eclipse & IntelliJ. Then we will move on to setup the Java development environment in you Mac machine, including
IntelliJ IDEA and OpenJDK 11 installation. Finally, we will create a simple Java application, calling hello-world.

*Feel free to comment and let us know what kind video you would like to watch*
